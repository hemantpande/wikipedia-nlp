import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ConjunctionFilter {

    private final List<String> knownConjunctions;
    private final String whitespace = " ";

    public ConjunctionFilter() {
        this.knownConjunctions = Arrays.asList("and", "but", "for", "nor", "or", "so", "yet");
    }

    public List<String> filter(final String text) {

        return eliminateNoise(text.toLowerCase())
                .stream()
                .filter(word -> !knownConjunctions.contains(word.trim()))
                .collect(Collectors.toList());
    }

    private List<String> eliminateNoise(String text) {
        Matcher matcher = Pattern.compile("[\\w']+").matcher(text);
        List<String> words = new ArrayList<>();

        while (matcher.find()) {
            words.add(text.substring(matcher.start(), matcher.end()));
        }

        return words;
    }
}
