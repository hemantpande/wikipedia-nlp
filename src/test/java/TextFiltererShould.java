import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TextFiltererShould {

    @Test
    @DisplayName("Conjunctions should get filtered for string with consistent spaces")
    public void filter_conjunctions_when_text_is_evenly_spaced(){

        ConjunctionFilter filter = new ConjunctionFilter();

        assertEquals(Arrays.asList("bread", "jam", "taste", "good", "together"),
                filter.filter("Bread and jam taste good together."));
    }

    @Test
    @DisplayName("Conjunctions should get filtered for string with inconsistent spaces")
    public void filter_conjunctions_when_text_is_unevenly_spaced(){

        ConjunctionFilter filter = new ConjunctionFilter();

        assertEquals(Arrays.asList("bread", "jam", "taste", "good", "together"),
                filter.filter("Bread  and jam            taste  good      together."));
    }
}
